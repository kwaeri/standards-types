/**
 * SPDX-PackageName: kwaeri/standards-types
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// EXPORTS

/* NODEKIT Standard Types */
export type {
    KWAERI,
    NodeKitOptions,
    NodeKitConfigurationBits,
    NodeKitProjectBits,
    NodeKitProjectAuthorBits,
    NodeKitProjectLicenseBits
} from './standards/nodekit.mjs';

/* JSON-API Style Guide */
export type {
    ErrorMessageBits,
    ErrorBits,
    ParameterBits,
    DataBits,
    ItemsBits,
    RequestBits,
    ResponseBits
} from './standards/json-api.mjs';

